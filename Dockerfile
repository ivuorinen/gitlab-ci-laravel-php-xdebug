FROM karbon001/gitlab-ci-laravel-php:latest

RUN docker-php-ext-install bcmath \
    && docker-php-ext-configure pcntl \
    && docker-php-ext-install pcntl

RUN php --version
RUN composer --version
RUN phpunit --version
RUN phpcs --version
